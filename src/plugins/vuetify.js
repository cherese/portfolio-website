import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import VImageInput from 'vuetify-image-input'

Vue.use(Vuetify)
Vue.component(VImageInput.name, VImageInput)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#b71c1c',
        secondary: '#383838',
        accent: '#A70000',
        error: '#ff0000',
        active: '#b71c1c'
      }
    }
  },
  icons: {
    iconfont: 'mdi'
  }
})
